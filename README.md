# Telegraf & InfluxDB

1. Start InfluxDB

    ```shell
    docker-compose up -d

    # Output like below ...
    Creating network "telegraf_demo_default" with the default driver
    Creating telegraf_demo_influxdb_1 ... done
    ```

2. Open admin page, setup!

    `http://localhost:8086`
